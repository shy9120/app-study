export const DB_STORE_NAME = 'app-study-db';
export const DB_VERSION = 1;
export const CACHE_KEY = 'app-study-cache';